
#include "devmap_functions.h"
#include "dijkstra.h"

//Function that uses optarg to shift through command line options, partially derived from MSG Simpson
void parse_args(int argc, char *argv[], int *temp_file_count, long *battery_life) {

	int c, checker;
	char *program = argv[0];

	while ((c = getopt(argc, argv, "p:")) != -1) {
		switch (c) {
			case 'p':
				if (optarg != NULL) {
					char *ptr; 
					*battery_life = strtol(optarg, & ptr, 10);
					if ( *ptr != '\0') {
						fprintf(stderr, "Error1: A number must be entered with -p usage:\n");
						fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
						exit(0);
					}
				}
				else {
					fprintf(stderr, "Error2: A number must be entered with -p usage:\n");
					fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
					exit(0);
				}

				if ( *battery_life < 0 || *battery_life > 100) {
					fprintf(stderr, "Error3: Battery value out of range:\n");
					fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
					exit(0);
				}
				break;
			case '?':
				fprintf(stderr, "Error4: A number must be entered with -p usage:\n");
				fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
				exit(0);
		}
	}

	*temp_file_count = optind;

	argc -= optind;
	argv += optind;

	if (argc < 1) {
		fprintf(stderr, "Error: At least one file argument must be provided.\n");
		fprintf(stderr, "Usage: %s -p <file.pcap>\n", program);
		exit(0);
	}

	for (int i = 0; i < argc; ++i) {
		if ((checker = access(argv[i], F_OK)) != 0) {
			fprintf(stderr, "ERROR: %s doesn't exist!\n", argv[i]);
			exit(0);
		}
	}
}

//Function that will step through the file and do all necessary checks while storing the required information
//in structures looping through until reaching the end of file
void check_is_valid(FILE *fp, hash *h){

	//Initializing structs
	struct global glo1 = {0};
	struct packet_header ph1 = {0};
	struct ethernet e1 = {0};
	struct udp u1 = {0};
	struct meditrik m1 = {0};
	struct status s1 = {0};
	struct gps g1;

	int global = 24, packetheader = 16, ethernet = 14, ipv4 = 20, ipv6 = 40, udp = 8, meditrik = 12;
	unsigned char *buff;
	unsigned int fileLen;
	
	fseek(fp, 0, SEEK_END);
	fileLen = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	buff = (unsigned char *) malloc(fileLen + 1);
	if (!buff) {
		perror("Memory error!\n");
		fclose(fp);
		exit(0);
	}
	
	fread(&glo1, global, 1, fp);
	if(glo1.magic_number != 0xA1B2C3D4){
	    free(buff);
		return;
	}
	
	while(!feof(fp)){
	
		if(fread(&ph1, packetheader, 1, fp) > 0){
			;
		}
		else{
		    free(buff);
			return;
		}

		fread(&e1, ethernet, 1, fp);
		ph1.incl_len -= ethernet;
		
		if(ntohs(e1.type) != 0x800 && ntohs(e1.type) != 0x86DD){
			fread(buff, ph1.incl_len, 1, fp);
			continue;
		}
		
		fread(buff, 1, 1, fp);
		fseek(fp, -1, SEEK_CUR);
		unsigned int version_type = buff[0] >> 4;
		if(version_type == 4 || version_type == 6){
			if(version_type == 4){
				ph1.incl_len -= ipv4;
				fread(buff, ipv4, 1, fp);
			}
			else{
				ph1.incl_len -= ipv6;
				fread(buff, ipv6, 1, fp);
			}
		}
		else{
			fread(buff, ph1.incl_len, 1, fp);
			continue;
		}
		
		fread(&u1, udp, 1, fp);
		ph1.incl_len -= udp;
		if (htons(u1.s_port) != 0xDEAD || htons(u1.d_port) != 0xDEAD) {
		    fread(buff, udp, 1, fp);
			continue;
		}
		
		fread(buff, 2, 1, fp);
		fseek(fp, -2, SEEK_CUR);
		fread(&m1, meditrik, 1, fp);
		ph1.incl_len -= meditrik;
		
		m1.version = (buff[0] >> 4);
		m1.sequenceID = ((buff[0] & 0xf) << 5) + (buff[1] >> 3);
		m1.type = (buff[1] & 7);
		
		m1.sourceID = ntohl(m1.sourceID);
		if (!hash_fetch(h, m1.sourceID)) {
			struct data *data = malloc(sizeof(*data));
			data->sourceID = m1.sourceID;
			data->lat = 0;
			data->lon = 0;
			data->alt = 0;
			data->batt = 0;
			data->batt_on = 0;
			hash_insert(h, data->sourceID, data);
		}
		
		check_type(&m1, &s1, &g1, &ph1, h, fp, buff);
	}
	free(buff);
}

//Function that will check the type, then exceuting a hash update if the type is 0 or 2, otherwise it will just move the fp along 
//to the next pcap header
int check_type(struct meditrik *m1, struct status *s1, struct gps *g1, struct packet_header *ph1, hash *h, FILE *fp, unsigned char *buff) {

	//Checking the type to decide which function will be called next
	if(m1->type == 0){
		s1->battery = 0;
		fread(s1, 8, 1, fp);
		fseek(fp, 6, SEEK_CUR);
		s1->battery *= 100;
		ph1->incl_len -= 14;
		if (hash_fetch(h, m1->sourceID)) {
			hash_update(h, m1->sourceID, m1, s1);
		}
	}
	else if (m1->type == 2) {
		fread(g1, 20, 1, fp);
		ph1->incl_len -= 20;
		if (hash_fetch(h, m1->sourceID)) {
			hash_update(h, m1->sourceID, m1, g1);
		}
	}
	else{
		fread(buff, ph1->incl_len, 1, fp);
	}
	return 1;
}

//Creates a copy of the target graph
graph *graph_copy(const graph *g){
	graph *z = graph_create();
	struct node *node = g->nodes;
	
	while(node){
		graph_add_node(z, node->data);
		node = node->next;
	}
	
	node = g->nodes;
	while(node){
		struct edge *test = node->edges;
		while(test){
			graph_add_edge(z, node->data, test->to->data, test->weight);
			test = test->next;
		}
		node = node->next;
	}
	return z;
}

//Function that checks to see if nodes are adjacent to each other, derived from SPC Paradis
bool is_adjacent(const graph *g, const struct data *a, const struct data *b)
{
	if(!g) {
		return false;
	}

	struct node *map = g->nodes;

	if(!map) {
		return false;
	}

	while(map) {
		if(map->data == a) {
			struct edge *edges = map->edges;

			while(edges) {
				if(edges->to->data == b) {
					return true;
				}
				edges = edges->next;
			}
			//return false;
		}
		map = map->next;
	}

	return false;
}

//Function that was created in class exercise
struct llist *graph_path(const graph *g, const void *from, const void *to) {
	hash *visited = hash_create();
	queue *to_process = queue_create();

	hash_insert(visited, (intptr_t)from, NULL);
	queue_enqueue(to_process, from);

	while (!queue_is_empty(to_process)) {
		void *curr = queue_dequeue(to_process);

		struct llist *adjacencies = graph_adjacent_to(g, curr);
		struct llist *check = adjacencies;
		while (check) {
			if (!hash_exists(visited, (intptr_t)check->data)) {
				hash_insert(visited, (intptr_t)check->data, curr);
				queue_enqueue(to_process, check->data);
				if (check->data == to) {
					ll_disassemble(adjacencies);
					goto FOUND;
				}
			}

			check = check->next;
		}

		ll_disassemble(adjacencies);
	}

	queue_disassemble(to_process);
	hash_disassemble(visited);
	return NULL;

	FOUND:
		queue_disassemble(to_process);

	struct llist *path = ll_create(to);
	while (hash_fetch(visited, ((struct data*)(path->data))->sourceID)) {
		ll_add(&path, hash_fetch(visited, ((struct data*)(path->data))->sourceID));
	}

	hash_disassemble(visited);

	return path;
}

//Function that was created in class exercise, altered to print the proper information
void print_item(const void *data, bool is_node) {
	if (is_node) {
		printf("\n%d", ((struct data*)data)->sourceID);
	}
	else {
		printf(u8" → %d", ((struct data*)data)->sourceID);
	}
}

//Function that was created in class exercise
void print_path(const struct llist *path) {
	while(path) {
		if(path->data){
			printf("\n%d → ", ((struct data *)path->data)->sourceID);
		}
		path = path->next;
	}
	printf("\n");
}

//Function that uses haversine to get the proper distances, derived from SPC Primm
double haversine(double lon2, double lon1, double lat2, double lat1){
    const int R = 6378100; 
    lon1 = lon1*(M_PI);
    lon2 = lon2*(M_PI);
    lat1 = lat1*(M_PI);
    lat2 = lat2*(M_PI);
    
    double dlon = lon2 - lon1;
    double dlat = lat2 - lat1;
    double a = pow(sin(dlat/2), 2) + cos(lat1) * cos(lat2) * pow((sin(dlon)/2), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = R * c;
    return d;
}

//Function that gets the correct measurments converted into meters that will return a number that 
//will help decide if a node gets added to the graph, derived from SPC Primm
double measurment(double lon2, double lon1, double lat2, double lat1, float alt1, float alt2){
    double a = 0, c = 0;
    float b = 0;
    a = haversine(lon2, lon1, lat2, lat1);
    
    alt1 = alt1*(1.8288);
    alt2 = alt2*(1.8288);
    
    if(alt1 > alt2){
    	b = alt1 - alt2;
    }
    else{
    	b = alt2 - alt1;
    }
    c = sqrt(a * a + b * b);
    return c;
}

//Function that prints out the device sourceID and battery
void print_if_not_zero(int sourceID, void *data, long battery_life){
	struct data *d = data;
	
	if(d->batt > -1 && d->batt <= battery_life && d->batt_on != 0){
		printf("Device #%u\n", sourceID);
		printf("Batt #%lf\n", d->batt);
	}
}

//Function that creats a graph, as well as calling sruballes algorithm to figure out if the graph made
//meets vendor recommendations, eventually printing the results to the screen before freeing and dissasenbling
//the used data
void make_graph(hash *h, long battery_life){
	graph *g = graph_create();
	void **things = malloc(sizeof(things) *50);
	int thing_tracker = 0;
	
	if(h){
		hash_traverse_graph(h, g, things, &thing_tracker);
		
		struct node *slow = g->nodes;
		
		while(slow){
			struct node *fast = slow->next;
			
			while(fast){
				const struct data *tmp = slow->data;
				const struct data *tmp2 = fast->data;
				double dist = measurment(tmp->lon, tmp2->lon, tmp->lat, tmp2->lat, tmp->alt, tmp2->alt);
				if(dist > 0.380 && dist <= 5.0 && tmp2->sourceID != tmp->sourceID){
					//printf("ASS WAFFLES!\n");
					graph_add_edge(g, slow->data, fast->data, dist);
					graph_add_edge(g, fast->data, slow->data, dist);
				}
				fast = fast->next;
			}
			slow = slow->next;
		}
	}

	size_t z = graph_node_count(g);
	size_t y = z/2;
	int have_i_been_here = 0;
	int printed = 0, i = 0;
	int *storage = malloc(sizeof(int *) * z);
	int situation = 0;

	while(z > y){
		int x = suurballees(g);
		
		if(x == 0){
			if(!printed){
				situation = 1;
			}
			break;
		}
		else{
			graph_remove_node(g, hash_fetch(h, x));
			storage[i] = x;
			++i;
			--z;
			if(have_i_been_here == 0){
				printed = 1;
				++have_i_been_here;
				situation = 2;
			}
		}
	}
	
	if(situation == 0){
		printf("Too many changes are needed to satisfy vendor requirements.\n");
	}
	else{
		if(situation == 1){
			printf("Network satisfies vendor recommendations.\n");
		}
		else{
			printf("Network Alterations:\n");
			for(int q = 0; q < i; ++q){
				printf("Remove device #%d\n", storage[q]);
			}
		}
	}
	
	printf("\nLow Battery (%ld%%):\n", battery_life);
	hash_traverse(h, battery_life, print_if_not_zero);
	
	free(storage);
	free(things);
	graph_disassemble(g);
}

void ll_print(struct llist *test)
{
	struct llist *tmp = test;
	
	printf("\n");
	while(tmp) {
		const struct device *data = tmp->data;
		printf("%d → ", ((struct data*)data)->sourceID);
		tmp = tmp->next;
	}
	printf("\n");
}

//Function that checks to see if the graph has any disjointed paths, and will return a 0 if it finds none
//otherwise returning the sourceID of a node that was part of a disjointed path, leading to its removal
int suurballees(graph *g){
	
	struct node *slow = g->nodes;
	struct llist *path = NULL;
	
	while(slow){
		struct node *fast = slow->next;
		while(fast){
			const struct data *tmp = slow->data;
			const struct data *tmp2 = fast->data;
			if(!is_adjacent(g, tmp, tmp2)){
				path = dijkstra_path(g, tmp, tmp2);
				if(!path){
					int sourceID = tmp->sourceID;
					ll_disassemble(path);
					return sourceID;
				}
				//ll_print(path);
				
				graph *copy = graph_copy(g);
				
				struct llist *head = path;
				path = path->next;
				while(path && path->next){
					graph_remove_node(copy, path->data);
					path = path->next;
				}
				
				path = dijkstra_path(copy, tmp, tmp2);
				if(!path){
					int sourceID = tmp->sourceID;
					graph_disassemble(copy);
					ll_disassemble(path);
					ll_disassemble(head);
					return sourceID;
				}
				//ll_print(path);
				
				graph_disassemble(copy);
				ll_disassemble(path);
				ll_disassemble(head);
			}
			fast = fast->next;
		}
		slow = slow->next;
	}
	printf("\n");
	return 0;
}

