
#ifndef M_PI
 #define M_PI acos(-1)
#endif

#include "hash.h"

#include "llist.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"
#include "decoder.h"
#include <math.h>

void print_item(const void *data, bool is_node);
void print_path(const struct llist *path);
double haversine(double lon2, double lon1, double lat2, double lat1);
double measurment(double lon2, double lon1, double lat2, double lat1, float alt1, float alt2);
void make_graph(hash *h, long battery_life);
int check_tpye(struct meditrik *m1, struct status *s1, struct gps *g1, hash *h);
void get_status(struct meditrik *m1, struct status *s1, hash *h);
void get_gps(struct meditrik *m1, struct gps *g1, hash *h);
int suurballees(graph *g);

bool is_adjacent(const graph *g, const struct data *a, const struct data *b)
{
	if(!g) {
		return false;
	}

	struct node *map = g->nodes;

	if(!map) {
		return false;
	}

	while(map) {
		if(map->data == a) {
			struct edge *edges = map->edges;

			while(edges) {
				if(edges->to->data == b) {
					return true;
				}
				edges = edges->next;
			}
			//return false;
		}
		map = map->next;
	}

	return false;
}

struct llist *graph_path(const graph *g, const void *from, const void *to) {
	hash *visited = hash_create();
	queue *to_process = queue_create();

	hash_insert(visited, (intptr_t)from, NULL);
	queue_enqueue(to_process, from);

	while (!queue_is_empty(to_process)) {
		void *curr = queue_dequeue(to_process);

		struct llist *adjacencies = graph_adjacent_to(g, curr);
		struct llist *check = adjacencies;
		while (check) {
			if (!hash_exists(visited, (intptr_t)check->data)) {
				hash_insert(visited, (intptr_t)check->data, curr);
				queue_enqueue(to_process, check->data);
				if (check->data == to) {
					ll_disassemble(adjacencies);
					goto FOUND;
				}
			}

			check = check->next;
		}

		ll_disassemble(adjacencies);
	}

	queue_disassemble(to_process);
	hash_disassemble(visited);
	return NULL;

	FOUND:
		queue_disassemble(to_process);

	struct llist *path = ll_create(to);
	while (hash_fetch(visited, ((struct data*)(path->data))->sourceID)) {
		ll_add(&path, hash_fetch(visited, ((struct data*)(path->data))->sourceID));
	}

	hash_disassemble(visited);

	return path;
}

//Got from http://www.linuxquestions.org/questions/programming-9/c-howto-read-binary-file-into-buffer-172985/
void ReadFile(int argc, char *argv[]) {

	int z = 0, count = 0; 
	unsigned char *buff;
	unsigned int fileLen;
	int file_count = 1;
	long battery_life = 5;
	int *temp_file_count;

	FILE *fp;

	temp_file_count = & file_count;
	parse_args(argc, argv, temp_file_count, & battery_life);

	hash *h = hash_create();

	for (; file_count < argc; ++file_count) {

		//Open file
		fp = fopen(argv[file_count], "r");
		if (!fp) {
			perror("Unable to open file.\n");
			exit(0);
		}
		
		check_is_valid(fp, buff, h);
		
	}
	make_graph(h, battery_life);
	hash_destroy(h);
}

void print_item(const void *data, bool is_node) {
	if (is_node) {
		//printf("\n%d", (char *) data);
		//printf("Latitude : %lf\n", ((struct data*)data)->lat);
		//printf("Longitude : %lf\n", ((struct data*)data)->lon);
		//printf("Altitude %f\n", ((struct data*)data)->alt*6);
		printf("\n%d", ((struct data*)data)->sourceID);
		//printf("\n");
	}
	else {
		printf(u8" → %d", ((struct data*)data)->sourceID);
	}
}

void print_path(const struct llist *path) {
	while(path) {
		if(path->data){
			printf("\n%d → ", ((struct data *)path->data)->sourceID);
		}
		path = path->next;
	}
	printf("\n");
}

void parse_args(int argc, char *argv[], int *temp_file_count, long *battery_life) {

	int c, checker;
	char *program = argv[0];

	while ((c = getopt(argc, argv, "p:")) != -1) {
		switch (c) {
			case 'p':
				if (optarg != NULL) {
					char *ptr; 
					*battery_life = strtol(optarg, & ptr, 10);
					if ( *ptr != '\0') {
						fprintf(stderr, "Error1: A number must be entered with -p usage:\n");
						fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
						exit(0);
					}
				}
				else {
					fprintf(stderr, "Error2: A number must be entered with -p usage:\n");
					fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
					exit(0);
				}

				if ( *battery_life < 0 || *battery_life > 100) {
					fprintf(stderr, "Error3: Battery value out of range:\n");
					fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
					exit(0);
				}
				break;
			case '?':
				fprintf(stderr, "Error4: A number must be entered with -p usage:\n");
				fprintf(stderr, "Usage: %s -p <int value 0-100> <file.pcap>\n", program);
				exit(0);
		}
	}

	*temp_file_count = optind;

	argc -= optind;
	argv += optind;

	if (argc < 1) {
		fprintf(stderr, "Error: At least one file argument must be provided.\n");
		fprintf(stderr, "Usage: %s -p <file.pcap>\n", program);
		exit(0);
	}

	for (int i = 0; i < argc; ++i) {
		if ((checker = access(argv[i], F_OK)) != 0) {
			fprintf(stderr, "ERROR: %s doesn't exist!\n", argv[i]);
			exit(0);
		}
	}
}

graph *graph_copy(const graph *g){
	graph *z = graph_create();
	struct node *node = g->nodes;
	
	while(node){
		graph_add_node(z, node->data);
		node = node->next;
	}
	
	node = g->nodes;
	while(node){
		struct edge *test = node->edges;
		while(test){
			graph_add_edge(z, node->data, test->to->data, test->weight);
			test = test->next;
		}
		node = node->next;
	}
	return z;
}

double haversine(double lon2, double lon1, double lat2, double lat1){
    const int R = 6378100; 
    lon1 = lon1*(M_PI);
    lon2 = lon2*(M_PI);
    lat1 = lat1*(M_PI);
    lat2 = lat2*(M_PI);
    
    double dlon = lon2 - lon1;
    double dlat = lat2 - lat1;
    double a = pow(sin(dlat/2), 2) + cos(lat1) * cos(lat2) * pow((sin(dlon)/2), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = R * c;
    return d;
}

double measurment(double lon2, double lon1, double lat2, double lat1, float alt1, float alt2){
    double a = 0, c = 0;
    float b = 0;
    a = haversine(lon2, lon1, lat2, lat1);
    
    alt1 = alt1*(1.8288);
    alt2 = alt2*(1.8288);
    
    if(alt1 > alt2){
    	b = alt1 - alt2;
    }
    else{
    	b = alt2 - alt1;
    }
    c = sqrt(a * a + b * b);
    return c;
}


void print_if_not_zero(int sourceID, void *data, long battery_life){
	struct data *d = data;
	
	if(d->batt > -1 && d->batt <= battery_life && d->batt_on != 0){
		printf("Device #%d\n", sourceID);
	}
}

void make_graph(hash *h, long battery_life){
	graph *g = graph_create();
	void **things = malloc(sizeof(things) *50);
	int thing_tracker = 0;
	
	if(h){
		hash_traverse_graph(h, g, things, &thing_tracker);
		
		struct node *slow = g->nodes;
		
		while(slow){
			struct node *fast = slow->next;
			
			while(fast){
				const struct data *tmp = slow->data;
				const struct data *tmp2 = fast->data;
				double dist = measurment(tmp->lon, tmp2->lon, tmp->lat, tmp2->lat, tmp->alt, tmp2->alt);
				if(dist > 0.380 && dist <= 5.0 && tmp2->sourceID != tmp->sourceID){
					//printf("ASS WAFFLES!\n");
					graph_add_edge(g, slow->data, fast->data, dist);
					graph_add_edge(g, fast->data, slow->data, dist);
				}
				fast = fast->next;
			}
			slow = slow->next;
		}
	}
	
	graph_print(g, print_item);
	//print_path(path);
	//printf("%zu\n", graph_edge_count(g));
	printf("\n");

	size_t z = graph_node_count(g);
	size_t y = z/2;
	int have_i_been_here = 0;

	while(z > y){
		int x = suurballe(g);
		
		if(x == 0){
			printf("Network satisfies vendor recommendations\n");
			break;
		}
		else{
			graph_remove_node(g, hash_fetch(h, x));
			--z;
			if(have_i_been_here == 0){
				printf("Network Alterations:\n");
				printf("Remove device #%d\n", x);
				++have_i_been_here;
			}
			else{
				printf("Remove device #%d\n", x);
			}
		}
	}
	
	printf("\nLow Battery (%ld%%):\n", battery_life);
	hash_traverse(h, battery_life, print_if_not_zero);
	
	free(things);
	graph_disassemble(g);
}

void ll_print(struct llist *test)
{
	struct llist *tmp = test;
	
	printf("\n");
	while(tmp) {
		const struct device *data = tmp->data;
		printf("%d → ", ((struct data*)data)->sourceID);
		tmp = tmp->next;
	}
	printf("\n");
}

int suurballees(graph *g){
	
	struct node *slow = g->nodes;
	struct llist *path = NULL;
	
	while(slow){
		struct node *fast = slow->next;
		while(fast){
			const struct data *tmp = slow->data;
			const struct data *tmp2 = fast->data;
			if(!is_adjacent(g, tmp, tmp2)){
				path = dijkstra_path(g, tmp, tmp2);
				if(!path){
					int sourceID = tmp->sourceID;
					ll_disassemble(path);
					return sourceID;
				}
				ll_print(path);
				
				graph *copy = graph_copy(g);
				
				struct llist *head = path;
				path = path->next;
				while(path && path->next){
					graph_remove_node(copy, path->data);
					path = path->next;
				}
				
				path = dijkstra_path(copy, tmp, tmp2);
				if(!path){
					int sourceID = tmp->sourceID;
					graph_disassemble(copy);
					ll_disassemble(path);
					ll_disassemble(head);
					return sourceID;
				}
				ll_print(path);
				
				graph_disassemble(copy);
				ll_disassemble(path);
				ll_disassemble(head);
			}
			fast = fast->next;
		}
		slow = slow->next;
	}
	printf("\n");
	return 0;
}

void check_is_valid(FILE *fp, char *buff, hash *h){

	//Initializing structs
	struct global glo1;
	struct packet_header ph1;
	struct ethernet e1;
	struct udp u1;
	struct meditrik m1;
	struct status s1;
	struct gps g1;

	int global = 24, packetheader = 16, ethernet = 14, ipv4 = 20, ipv6 = 40, udp = 8, omeditrik = 12;
	
	buff = (unsigned char *) malloc(fileLen + 1);
	if (!buff) {
		perror("Memory error!\n");
		fclose(fp);
		exit(0);
	}
	
	fread(glo1, global, 1, fp);
	if(glo1.magic_number != 0xA1B2C3D4){
		return;
	}
	
	grab_packet_header:
	
		if(fread(buff, packetheader, 1, fp) != 0){
			ph1.len -= packetheader;
		}
		else{
			return;
		}
		
		fread(e1, ethernet, 1, fp);
		ph1.len -= ethernet;
		if(e1.type != 1){
			//fseek(fp, ph1.len, SEEK_CUR);
			fread(buff, ph1.len, 1, fp);
			ph1.len -= ethernet;
			goto grab_packet_header;
		}
		
		fread(buff, 4, 1, fp);
		unsigned int version_type = buff[0] >> 4;
		if(version_type == 4 || version_type == 6){
			if(version_type == 4){
				fseek(fp, -4, SEEK_CUR);
				ph1.len -= ipv4;
				fread(buff, ph1.len, 1, fp);
			}
			else{
				fseek(fp, -4, SEEK_CUR);
				ph1.len -= ipv6;
				fread(buff, ph1.len, 1, fp);
			}
		}
		else{
			fread(buff, ph1.len, 1, fp);
			goto grab_packet_header;
		}
		
		fread(u1, udp, 1, fp);
		ph1.len -= udp;
		if (u1.s_port != 57005 || u1.d_port != 57005) {
			return;
		}
		else{
			fread(buff, ph1.len, 1, fp);
		}
		
		fread(m1, meditrik, 1, fp);
		ph1.len -= meditrik;
		if (!hash_fetch(h, m1->sourceID)) {
			struct data *data = malloc(sizeof(*data));
			data->sourceID = m1->sourceID;
			data->lat = 0;
			data->lon = 0;
			data->alt = 0;
			data->batt = 0;
			data->batt_on = 0;
			//printf("%d\n", m1->sourceID);
			hash_insert(h, m1->sourceID, data);
		}
		
		check_tpye(&m1, &s1, &g1, &ph1, h, fp, buff);
}

int check_tpye(struct meditrik *m1, struct status *s1, struct gps *g1, struct packet_header *ph1, hash *h, FILE *fp, char *buff) {

	//Checking the type to decide which function will be called next
	if(m1->type == 0){
		fread(s1, 8, 1, fp);
		ph1.len -= 8;
		if (hash_fetch(h, m1->sourceID)) {
			hash_update(h, m1->sourceID, m1, s1);
		}
		fread(buff, ph1.len, 1, fp);
	}
	else if (m1->type == 2) {
		fread(g1, 20, 1, fp);
		ph1.len -= 20;
		if (hash_fetch(h, m1->sourceID)) {
			hash_update(h, m1->sourceID, m1, g1);
		}
		fread(buff, ph1.len, 1, fp);
	}
	return 1;
}



