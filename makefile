CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

devmap: devmap.o devmap_functions.o llist.o hash.o ll_queue.o heap.o graph.o dijkstra.o -lm

man:
	sudo cp devmap.1 /usr/share/man/man1/

.PHONY: clean debug

clean:
	-rm *.o

debug: CFLAGS+=-g
debug: devmap

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: devmap