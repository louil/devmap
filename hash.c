
#define _BSD_SOURCE
#include "hash.h"
#include "decoder.h"

#include <stdint.h> 
#include <string.h>

// static so that it will only be seen
// in this compilation unit
static const size_t DEFAULT_SIZE = 8;
static const double LOAD_FACTOR = 0.7;

static hash *__hash_create(size_t capacity);
static uint64_t __wang_hash(uint64_t key);

static void h_llist_disassemble(struct h_llist *list);
static void h_llist_destroy(struct h_llist *list);

static struct h_llist *h_llist_create(unsigned int key, void *value);
static size_t hashish(unsigned int key, size_t capacity);

// Negative returns imply error
// 0 return implies no work taken
// positive implies hash resized
static int hash_recalculate(hash *h);

//Function that was created in class exercise
hash *hash_create(void) {
	return __hash_create(DEFAULT_SIZE);
}

//Function that was created in class exercise
void hash_disassemble(hash *h) {
	if (h) {
		for (size_t n = 0; n < h->capacity; ++n) {
			h_llist_disassemble(h->data[n]);
		}

		free(h->data);
		free(h);
	}
}

//Function that was created in class exercise
void hash_destroy(hash *h) {
	if (h) {
		for (size_t n = 0; n < h->capacity; ++n) {
			h_llist_destroy(h->data[n]);
		}

		free(h->data);
		free(h);
	}
}

//Function that was created in class exercise
bool hash_insert(hash *h, unsigned int key, void *value) {
	if (!h || !key) {
		return false;
	}

	size_t idx = hashish(key, h->capacity);
	// Search the hash for the value to update
	struct h_llist *tmp = h->data[idx];
	while (tmp) {
		if (tmp->key == key) {
			tmp->value = value;
			return true;
		}
		tmp = tmp->next;
	}

	// If the load factor is hit, rebuild the hash,
	// and recalculate the index
	int val = hash_recalculate(h);
	if (val < 0) {
		return false;
	}
	else if (val > 0) {
		idx = hashish(key, h->capacity);
	}

	struct h_llist *new = h_llist_create(key, value);
	if (!new) {
		return false;
	}

	// Add new value to head of linked list
	new->next = h->data[idx];
	h->data[idx] = new;
	h->item_count += 1;

	return true;
}

//Function that was created in class exercise
void *hash_fetch(hash *h, unsigned int key) {
	if (!h || !key) {
		return NULL;
	}

	size_t idx = hashish(key, h->capacity);
	struct h_llist *tmp = h->data[idx];

	while (tmp) {
		if (tmp->key == key) {
			return tmp->value;
		}

		tmp = tmp->next;
	}

	return NULL;
}

//Function that was created in class exercise
bool hash_exists(hash *h, unsigned int key) {
	if (!h || !key) {
		return false;
	}

	size_t idx = hashish(key, h->capacity);
	struct h_llist *tmp = h->data[idx];

	while (tmp) {
		if (tmp->key == key) {
			return true;
		}

		tmp = tmp->next;
	}

	return false;
}

//Function that was created in class exercise
static int hash_recalculate(hash *h) {
	if (h->item_count < LOAD_FACTOR *h->capacity) {
		return 0;
	}

	hash *cpy = __hash_create(h->capacity *2);
	if (!cpy) {
		return -2;
	}

	for (size_t n = 0; n < h->capacity; ++n) {
		struct h_llist *tmp = h->data[n];
		while (tmp) {
			hash_insert(cpy,
				tmp->key, tmp->value);
			tmp = tmp->next;
		}
	}

	for (size_t n = 0; n < h->capacity; ++n) {
		h_llist_disassemble(h->data[n]);
	}
	free(h->data);

	h->capacity = cpy->capacity;
	h->item_count = cpy->item_count;
	h->data = cpy->data;

	free(cpy);

	return 1;
}

//Function that was created in class exercise, altered to also take battery_life 
void hash_traverse(hash *h,	long battery_life, void( *func)(int, void *, long battery_life)) {
	for (size_t n = 0; n < h->capacity; ++n) {
		struct h_llist *tmp = h->data[n];
		while (tmp) {
			func(tmp->key, tmp->value, battery_life);
			tmp = tmp->next;
		}
	}
}

//Function that takes the passed information and then inserts into a graph
void hash_traverse_graph(hash *h, graph *g, void **things, int *thing_tracker){
	for (size_t n = 0; n < h->capacity; ++n) {
		struct h_llist *tmp = h->data[n];
		while (tmp) {
			graph_add_node(g, tmp->value);
			things[*thing_tracker] = tmp->value;
			++(*thing_tracker);
			tmp = tmp->next;
		}
	}
}

//Function that was created in class exercise
static hash *__hash_create(size_t capacity) {
	hash *h = malloc(sizeof( *h));
	if (!h) {
		return NULL;
	}

	h->data = calloc(capacity,
		sizeof( *h->data));
	if (!h->data) {
		free(h);
		return NULL;
	}

	h->capacity = capacity;
	h->item_count = 0;
	return h;
}

//Function that was created in class exercise
static uint64_t __wang_hash(uint64_t key) {
	key = (~key) + (key << 21); // key = (key << 21) - key - 1;
	key = key ^ (key >> 24);
	key = (key + (key << 3)) + (key << 8); // key *265
	key = key ^ (key >> 14);
	key = (key + (key << 2)) + (key << 4); // key *21
	key = key ^ (key >> 28);
	key = key + (key << 31);
	return key;
}

//Function that was created in class exercise
static void h_llist_disassemble(struct h_llist *list) {
	while (list) {
		struct h_llist *tmp = list->next;
		free(list);
		list = tmp;
	}
}

//Function that was created in class exercise
static void h_llist_destroy(struct h_llist *list) {
	while (list) {
		struct h_llist *tmp = list->next;
		free(list->value);
		free(list);
		list = tmp;
	}
}

//Function that was created in class exercise
static struct h_llist *h_llist_create(unsigned int key, void *value) {
	struct h_llist *node = malloc(sizeof( *node));
	if (!node) {
		return NULL;
	}

	node->key = key;
	if (!node->key) {
		free(node);
		return NULL;
	}
	node->value = value;
	node->next = NULL;

	return node;
}

//Function that was created in class exercise
static size_t hashish(unsigned int key, size_t capacity) {
	uint64_t buf = key;

	return __wang_hash(buf) % capacity;
}

//Function that was takes the passed information and then checks the type, if the type is 0 it calls hash_update_bat, otherwise if
//type is 2 it calls hash_update_gps
void hash_update(hash *h, unsigned int key, struct meditrik *m1, const void *data) {
	if (!h || !key) {
		return;
	}

	if (m1->type == 0) {
		hash_update_bat(h, key, (void *)data);
	}
	else if (m1->type == 2) {
		hash_update_gps(h, key, (void *)data);
	}
}

//Function that takes the passed information and then updates the gps information
void hash_update_gps(hash *h, unsigned int key, struct gps *g1) {
	if (!h || !key) {
		return;
	}

	size_t idx = hashish(key, h->capacity);
	struct h_llist *tmp = h->data[idx];
	while (tmp) {
		if (tmp->key == key) {
			((struct data*)tmp->value)->lat = g1->latitude;
			((struct data*)tmp->value)->lon = g1->longitude;
			((struct data*)tmp->value)->alt = g1->altitude;
			return;
		}
		tmp = tmp->next;
	}
}

//Function that takes the passed information and updates the battery information and turns the batt_on flag on so that 
//later on the program can determine if battery information was ever added 
void hash_update_bat(hash *h, unsigned int key, struct status *s1) {
	if (!h || !key) {
		return;
	}
	size_t idx = hashish(key, h->capacity);
	struct h_llist *tmp = h->data[idx];
	while (tmp) {
		if (tmp->key == key) {
			((struct data*)tmp->value)->batt = s1->battery;
			((struct data*)tmp->value)->batt_on = 1;
			return;
		}
		tmp = tmp->next;
	}
}