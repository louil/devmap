
#include "devmap_functions.h"
#include "decoder.h"

int main(int argc, char *argv[]){
    //int z = 0, count = 0; 
	int file_count = 1;
	long battery_life = 5;
	int *temp_file_count;

	FILE *fp;

	temp_file_count = & file_count;
	
	//Function that uses optarg to shift through command line options, partially derived from MSG Simpson 
	parse_args(argc, argv, temp_file_count, &battery_life);

	hash *h = hash_create();

	//Loops through all the files provided on the command line and hits the check_is_valid function
	for (; file_count < argc; ++file_count) {
		fp = fopen(argv[file_count], "r");
		if (!fp) {
			perror("Unable to open file.\n");
			exit(0);
		}
		
		//Function that will step through the file and do all necessary checks while storing the required information
		//in structures looping through until reaching the end of file
		check_is_valid(fp, h);
		
		fclose(fp);
	}
	
	//Function that creats a graph, as well as calling sruballes algorithm to figure out if the graph made
	//meets vendor recommendations, eventually printing the results to the screen before freeing and dissasenbling
	//the used data
	make_graph(h, battery_life);
	hash_destroy(h);
}



