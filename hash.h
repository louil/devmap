
#ifndef HASH_H
 #define HASH_H
 
#include "decoder.h"
#include "graph.h"

#include <stdbool.h>
#include <stdlib.h>

//Declaring structs to be used, created in class with slight alterations to work with the program
struct h_llist {
	unsigned int key;
	void *value;
	struct h_llist *next;
};

struct _hash {
	size_t item_count;
	size_t capacity;
	struct h_llist **data;
};

typedef struct _hash hash;

hash *hash_create(void);
void hash_destroy(hash *h);
void hash_disassemble(hash *h);

bool hash_insert(hash *h, unsigned int key, void *value);
bool hash_remove(hash *h, unsigned int key);

bool hash_exists(hash *h, unsigned int key);
void *hash_fetch(hash *h, unsigned int key);

void hash_traverse(hash *h, long battery_life, void (*func)(int , void *, long battery_life));
void hash_traverse_graph(hash *h, graph *g, void **things, int *thing_tracker);
void hash_update(hash *h, unsigned int key, struct meditrik *m1, const void *data);
void hash_update_gps(hash *h, unsigned int key, struct gps *g1);
void hash_update_bat(hash *h, unsigned int key, struct status *s1);

#endif
