#ifndef M_PI
 #define M_PI acos(-1)
#endif

#include "hash.h"
#include "llist.h"
#include "heap.h"
#include "queue.h"
#include "graph.h"
#include "decoder.h"
#include <math.h>

void check_is_valid(FILE *fp, hash *h);
void print_item(const void *data, bool is_node);
void print_path(const struct llist *path);
double haversine(double lon2, double lon1, double lat2, double lat1);
double measurment(double lon2, double lon1, double lat2, double lat1, float alt1, float alt2);
void make_graph(hash *h, long battery_life);
int check_type(struct meditrik *m1, struct status *s1, struct gps *g1, struct packet_header *ph1, hash *h, FILE *fp, unsigned char *buff);
int suurballees(graph *g);