
#ifndef DECODER_H
 #define DECODER_H

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <inttypes.h>
#include <math.h>

//Declaring the structs that will store the proper pcap information
struct global{
	unsigned int magic_number: 32;
	unsigned int version_major: 16; 
	unsigned int version_minor: 16; 
	unsigned int time_zone: 32; 
	unsigned int sig_flags: 32; 
	unsigned int snap_len: 32; 
	unsigned int network: 32;
};

struct packet_header{
	unsigned int ts_sec: 32;
	unsigned int ts_usec: 32;
	unsigned int incl_len: 32;
	unsigned int len: 32;

};

struct __attribute__((packed)) ethernet{
	unsigned int destmac: 32;
	unsigned int destmac_split: 16;
	unsigned int sourcemac: 32;
	unsigned int sourcemac_split: 16;
	unsigned int type: 16;
};

struct udp{
	unsigned int s_port: 16;
	unsigned int d_port: 16;
};

struct meditrik{
	unsigned int version: 4;
	unsigned int sequenceID: 9;
	unsigned int type: 3;
	unsigned int totalLength: 16;
	unsigned int sourceID: 32;
	unsigned int destinationID: 32;
};

struct status{
	double battery;
};

struct gps{
	double latitude;
	double longitude;
	float altitude;
	
};

struct data{
	double lat;
	double lon;
	float alt;
	double batt;
	int batt_on;
	int sourceID;
};

void ReadFile(int argc, char *argv[]);
void parse_args(int argc, char *argv[], int *temp_file_count, long *battery_life);

#endif
